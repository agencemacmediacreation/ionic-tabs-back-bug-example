import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Subpage2Page } from './subpage2.page';

describe('Subpage2Page', () => {
  let component: Subpage2Page;
  let fixture: ComponentFixture<Subpage2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Subpage2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Subpage2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
