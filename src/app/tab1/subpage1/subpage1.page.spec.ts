import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Subpage1Page } from './subpage1.page';

describe('Subpage1Page', () => {
  let component: Subpage1Page;
  let fixture: ComponentFixture<Subpage1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Subpage1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Subpage1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
