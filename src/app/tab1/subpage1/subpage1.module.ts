import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Subpage1PageRoutingModule } from './subpage1-routing.module';

import { Subpage1Page } from './subpage1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Subpage1PageRoutingModule
  ],
  declarations: [Subpage1Page]
})
export class Subpage1PageModule {}
