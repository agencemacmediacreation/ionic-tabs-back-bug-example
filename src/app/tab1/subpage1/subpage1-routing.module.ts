import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Subpage1Page } from './subpage1.page';

const routes: Routes = [
  {
    path: '',
    component: Subpage1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Subpage1PageRoutingModule {}
